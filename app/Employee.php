<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    // Table Name
    protected $employees = 'employees';

    public function fetchEmployees(){
        $res = Employee::all();
        return $res;
    }

    public function addEmployee($request){
        $employee= [
            ['name' => $request->input('name'),
            'platform' => $request->input('platform')],
        ];
        $res = Employee::insert($employee);

        return $employee;
    }

    public function fetchEmployee($id){
        $res = Employee::where('id',$id)->get();
        return $res;
    }

    public function updateEmployee($request,$id){
        $employee= ['name' => $request->input('name'),
                    'platform' => $request->input('platform')];

        $res = Employee::where('id',$id)->update($employee);
        return $employee;
    }

    public function deleteEmployee($id){
        $res = Employee::where('id','=',$id)->delete();
        return $res;
    }

}
